import React, { FC } from "react";
import { ArticleJsonLd, NextSeo } from "next-seo";
import { useRouter } from "next/router";

// type LayoutProps = {
//   slug: string,
//   frontMatter: {
//     title: string,
//     author?: string,
//     authorUrl?: string,
//     authorAvatarUrl?: string,
//     excerpt?: string,
//     publishedAt?: string,
//     readingTime?: {
//       text: string,
//       time: number,
//       words: number,
//       minutes: number,
//     },
//     views?: string,
//     image?: string,
//     headings?: Headings,
//     showHeadings?: 0 | 1 | 2,
//     showHeadingsExpanded?: boolean,
//   },
// };

export const Layout = ({
  children,
  slug,
  frontMatter: {
    title,
    author = "",
    authorAvatarUrl,
    publishedAt = Date.now().toString(),
    views,
    readingTime,
    excerpt,
    image,
    headings,
    showHeadings = 0,
    showHeadingsExpanded = false,
  },
}) => {
  const router = useRouter();
  let canonical = `/${slug}`;
  if (!slug) {
    canonical = `${router.pathname}`;
  }

  return (
    <>
      <NextSeo
      // title={`${title} – Felix Tellmann`}
      // description={excerpt}
      // canonical={canonical}
      // openGraph={{
      //   type: "article",
      //   article: {
      //     publishedTime: new Date(publishedAt).toISOString(),
      //   },
      //   url: canonical,
      //   title,
      //   description: excerpt,
      //   images: [
      //     {
      //       url: `https://felixtellmann.com${image}`,
      //       alt: title,
      //     },
      //   ],
      // }}
      />
      <ArticleJsonLd
      // authorName="Felix Tellmann"
      // dateModified={new Date(publishedAt).toISOString()}
      // datePublished={new Date(publishedAt).toISOString()}
      // description={excerpt}
      // images={[`https://felixtellmann.com${image}`]}
      // publisherLogo="/favicons/android-icon-192x192.png"
      // publisherName="Felix Tellmann"
      // title={title}
      // url={canonical}
      />
    </>
  );
};

export default Layout;
