// module.exports = {
//   setupFiles: ["<rootDir>/jest.setup.js"],
//   testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/build/"],
//   moduleFileExtensions: [
//     "js",
//     "json",
//     // 'scss'
//   ],
//   moduleNameMapper: {
//     "pages(.*)$": "<rootDir>/pages$1",
//     "containers(.*)$": "<rootDir>/containers$1",
//     "components(.*)$": "<rootDir>/components$1",
//     // 'styles(.*)$': '<rootDir>/styles$1',
//     "enums(.*)$": "<rootDir>/enums$1",
//     "tools(.*)$": "<rootDir>/tools$1",
//     "\\.(css|less|sass|scss)$": "identity-obj-proxy",
//   },
//   verbose: false,
// };
// module.exports = {
//   collectCoverageFrom: ["**/*.{ts,tsx}", "!**/*.d.ts", "!**/node_modules/**"],
//   setupFilesAfterEnv: ["<rootDir>/setupTests.ts"],
//   testPathIgnorePatterns: ["/node_modules/", "/.next/"],
//   transform: {
//     "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
//   },
//   transformIgnorePatterns: ["/node_modules/"],
// };
