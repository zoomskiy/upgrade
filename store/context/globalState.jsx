import React, { createContext, useReducer, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import { v4 } from "uuid";
import reducers from "./reducers";
import { getData } from "../../utils/fetchData";
import { ACTIONS } from "./actions";
import Notification from "components/Notification";

export const DataContext = createContext();

export const DataProvider = ({ children, token, user }) => {
  const initialState = {
    notify: {},
    // auth: {},
    auth: token && user ? { user, token } : {},
    cart: [],
    modal: [],
    orders: [],
    users: [],
    categories: [],
    notifications: [],
  };
  const router = useRouter();
  const [state, dispatch] = useReducer(reducers, initialState);
  const { cart, auth } = state;

  // ПЕРЕДАЛАНО: => Проверям пользователя на сервере
  // useEffect(() => {
  //   (async () => {
  //     dispatch({ type: ACTIONS.NOTIFY, payload: { loading: true } });
  //     const firstLogin = localStorage.getItem("firstLogin");
  //     if (firstLogin) {
  //       getData("auth/accessToken").then((res) => {
  //         if (res.err) {
  //           dispatch({ type: ACTIONS.NOTIFY, payload: { loading: false } });
  //           return localStorage.removeItem("firstLogin");
  //         }

  //         dispatch({
  //           type: ACTIONS.AUTH,
  //           payload: {
  //             token: res.access_token,
  //             user: res.user,
  //           },
  //         });
  //         // router.push("/");
  //         dispatch({ type: ACTIONS.NOTIFY, payload: { loading: false } });
  //       });
  //     }
  //     dispatch({ type: ACTIONS.NOTIFY, payload: { loading: false } });
  //   })();
  // }, []);

  //     getData('categories').then(res => {
  //         if(res.err) return dispatch({type: 'NOTIFY', payload: {error: res.err}})

  //         dispatch({
  //             type: "ADD_CATEGORIES",
  //             payload: res.categories
  //         })
  //     })

  // },[])

  useEffect(() => {
    const __next__cart01__devat = JSON.parse(localStorage.getItem("__next__cart01__devat"));

    if (__next__cart01__devat) dispatch({ type: ACTIONS.ADD_CART, payload: __next__cart01__devat });
  }, []);

  useEffect(() => {
    localStorage.setItem("__next__cart01__devat", JSON.stringify(cart));
  }, [cart]);

  useEffect(() => {
    if (auth.token) {
      getData("order", auth.token).then((res) => {
        if (res.err) return dispatch({ type: "NOTIFY", payload: { error: res.err } });

        dispatch({ type: "ADD_ORDERS", payload: res.orders });
      });

      if (auth.user.role === "admin") {
        getData("user", auth.token).then((res) => {
          if (res.err) return dispatch({ type: "NOTIFY", payload: { error: res.err } });

          dispatch({ type: "ADD_USERS", payload: res.users });
        });
      }
    } else {
      dispatch({ type: "ADD_ORDERS", payload: [] });
      dispatch({ type: "ADD_USERS", payload: [] });
    }
  }, [auth.token]);

  return (
    <DataContext.Provider value={{ state, dispatch }}>
      <>
        <div className="notification-wrapper">
          {state.notifications.map((note, index) => {
            return <Notification dispatch={dispatch} key={note.id} {...note} />;
          })}
        </div>

        {children}
      </>
    </DataContext.Provider>
  );
};

export const useNotification = () => {
  const { dispatch } = useContext(DataContext);
  return (props) => {
    dispatch({
      type: "ADD_NOTOFICATION",
      payload: {
        id: v4(),
        ...props,
      },
    });
  };
};
