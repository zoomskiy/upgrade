import React from "react";

const Meta = () => (
  <>
    <meta
      name="description"
      content="Infotelligent, your trusted B2B contact database provider, offers accurate business contact lookup solutions empowering companies to grow, smarter."
    />
    <meta property="og:url" content="" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Supercharged B2B Prospecting Database" />
    <meta
      property="og:description"
      content="B2B contact database provider for Demand Generation, Sales Planning, and Marketing Operations."
    />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Supercharged B2B Prospecting Database" />
    <meta
      name="twitter:description"
      content="B2B contact database provider for Demand Generation, Sales Planning, and Marketing Operations."
    />
    <meta name="twitter:image" content="" />
    <meta property="og:image" content="" />
  </>
);

export default Meta;
