const path = require("path"); // стандартный модуль предназначенный для работы с путями к файлам
//dirname() — возвращает родительскую директорию файла.
// basename() — возвращает имя файла.
// extname() — возвращает расширение файла.
const { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } = require("next/constants");
const webpackConfig = require("./webpack.config");

module.exports = (phase) => {
  // при запуске в режиме разработки `next dev` или` npm run dev` независимо от значения переменной среды STAGING
  const isDev = phase === PHASE_DEVELOPMENT_SERVER;
  // когда используется `next build` или` npm run build`
  const isProd = phase === PHASE_PRODUCTION_BUILD;
  // const isProd =
  //   phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== "1";
  // когда используется `next build` или` npm run build`
  const isStaging = phase === PHASE_PRODUCTION_BUILD;
  // const isStaging =
  //   phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === "1";
  // console.log(`isDev:${isDev}  isProd:${isProd}   isStaging:${isStaging}`);

  const env = {
    APITEST: (() => {
      if (isDev) return "localhost: 3000";
      if (isProd) return "В проде";
      if (isStaging) return "isStaging";
      return "(isDev,isProd && !isStaging,isProd && isStaging)";
    })(),

    RESTURL_SPEAKERS: (() => {
      if (isDev) return "В разработке";
      if (isProd) return "В проде";
      if (isStaging) return "isStaging";
      return "(isDev,isProd && !isStaging,isProd && isStaging)";
    })(),

    RESTURL_SESSIONS: (() => {
      if (isDev) return "В разработке";
      if (isProd) return "В проде";
      if (isStaging) return "isStaging";
      return "(isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    BASE_URL: `${process.env.BASE_URL}`,
    MONGODB_URL:
      `${process.env.MONGODB_URL}`,
    ACCESS_TOKEN_SECRET: `${process.env.ACCESS_TOKEN_SECRET}`,
    SECRET_KEY: `${process.env.SECRET_KEY}`,
    REFRESH_TOKEN_SECRET: `${process.env.REFRESH_TOKEN_SECRET}`,
    PAYPAL_CLIENT_ID: "SMT",
    CLOUD_UPDATE_PRESET: `${process.env.CLOUD_UPDATE_PRESET}`,
    CLOUD_NAME: `${process.env.CLOUD_NAME}`,
    CLOUD_API: `${process.env.CLOUD_API}`,
  };
console.log("test")
  // объект next.config.js
  return {
    i18n: {
      locales: ["en-US", "ru-RU"],
      defaultLocale: "ru-RU",
    },
    sassOptions: {
      includePaths: [path.join(__dirname, "styles")],
    },
    env,
    pageExtensions: ["jsx", "js", "ts", "tsx"],
    webpack: webpackConfig,
    compress: false,
    poweredByHeader: false,
    generateEtags: false,
    generateBuildId: async () => {
      // Вы можете, например, получить последний хеш коммита git здесь
      return "my-build-id";
    },
    eslint: {
      // TRUE ? Это может замедлить компиляцию страниц во время разработки : Norm

      dev: false,
    },
    reactStrictMode: true,
  };
};
