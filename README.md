Templates:

- https://github.com/100BC/nextjs-template

## Getting Started

` yarn && yarn dev`

Open browser and navigate to [http://localhost:3000/](http://localhost:3000/)

## Linters

```bash
# Вы можете использовать расширения VSCode ESLint и StyleLint для автоматического линтинга.

npm eslint . --ext .js,.jsx,.ts,.tsx

npm stylelint "styles/**/*.scss"
```

### Formatting

```bash
# автоматически исправляет файлы по правилам

# можетe включить автоматический формат для Prettier через VSCode Extensions
npm prettier --write .

# Prettier will not reorder based on rules
npm stylelint "styles/**/*.scss" --fix
```
