const valid = (username, email, password, password2, gender, fullname) => {
  if (!username || !fullname || !email || !password) return "Пожалуйста заполните поля";

  if (!validateEmail(email)) return "Некорректная почта";

  if (password.length < 6) return "Пароль должен состоять не менее 6 символов";

  if (password !== password2) return "Пароли не совпадают";
};

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default valid;
