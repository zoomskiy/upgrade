export const imageUpload = async (images) => {
  let imgArr = [];
  for (const item of images) {
    const formData = new FormData();
    formData.append("file", item);
    formData.append("upload_preset", process.env.CLOUD_UPDATE_PRESET);
    formData.append("cloud_name", process.env.CLOUD_NAME);

    const res = await fetch(process.env.CLOUD_API, {
      method: "POST",
      body: formData,
    });
    console.log(process.env.CLOUD_UPDATE_PRESET, process.env.CLOUD_NAME, process.env.CLOUD_API);
    const data = await res.json();
    imgArr.push({ public_id: data.public_id, url: data.secure_url });
  }
  return imgArr;
};
