require("dotenv").config();
const mongoose = require("mongoose");
const logger = require("morgan");
const express = require("express");
const next = require("next");
const port = parseInt(process.env.PORT, 10) || 1337;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const { MONGODB_URL } = process.env;
const helmet = require("helmet");
const { generateMD5 } = require("../utils/generateHash");
// const cookieParser = require("cookie-parser");
const Orders = require("../models/OrderModel");
const Users = require("../models/UserModal");
// const { date } = require("yup/lib/locale");
const cors = require("cors");
const RoomController = require("./controllers/RoomControler");
const Cookies = require("nookies");
const cookieParser = require("cookie-parser");
const jwt = require("jsonwebtoken");
const Room = require("../models/RoomModel");
// const auth = async (req, res) => {
//   // const token = req.headers.authorization;
//   // const token = req.headers.cookie;
//   try {
//     const token = req.cookies.refreshtoken;
//     if (!token) return res.status(400).json({ err: "Ошибка авторизации" });
//     console.log(token, "token");
//     const decoded = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
//     if (!decoded) return res.status(400).json({ err: "Ошибка авторизации" });

//     const user = await Users.findOne({ _id: decoded.id });

//     return { id: user._id, role: user.role, root: user.root };
//   } catch (error) {
//     return res.status(400).json({ err: "Ошибка авторизации" });
//   }
// };

app
  .prepare()
  .then(() => {
    const server = express();

    // server.use(helmet());
    // server.use(helmet.contentSecurityPolicy());

    server.use(
      helmet.contentSecurityPolicy({
        directives: {
          ...helmet.contentSecurityPolicy.getDefaultDirectives(),
          "default-src": ["*"],
          "img-src": ["self", "*.cloudinary.com", "data:", "blob:", "image:"],
          "script-src": [
            "'self'",
            "'unsafe-inline'",
            "'unsafe-eval'",
            "nonce-a",
            "blob:",
            "filesystem: *",
          ],
        },
      })
      // helmet.referrerPolicy({
      //   policy: 'no-referrer',
      // }),
      // helmet.expectCt({
      //   maxAge: 86400,
      // }),
      // helmet.hsts(),
      // helmet.noSniff(),
      // helmet.dnsPrefetchControl(),
      // helmet.ieNoOpen(),
      // helmet.frameguard(),
      // helmet.xssFilter(),
    );

    server.use(cors());
    server.use(express.json({ limit: "5MB" }));
    server.use(cookieParser());
    mongoose
      .connect(MONGODB_URL, {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        console.log("Подключились к бд %s", MONGODB_URL);
      })
      .catch((err) => {
        console.error("Ошибка при подключении к бд:", err.message);
        process.exit(1);
      });

    if (process.env.NODE_ENV !== "production") {
      server.use(logger("dev"));
    }

    server.get("/rooms", async (req, res) => {
      try {
        const rooms = await Room.find({});
        const actualPage = "/rooms";
        const queryParams = {};
        req.body = { rooms: JSON.stringify(rooms) };
        app.render(req, res, actualPage, queryParams);
        // res.status(200).json({ items });
      } catch (error) {
        console.log(error);
        return res.status(400).json({ err: "Ошибка" });
      }
    });

    // server.get("/rooms", RoomController.index);
    server.post("/rooms", RoomController.create);
    // Можно использовать regexp в url,некая валидация
    // server.get("/rooms/:id", RoomController.show);
    server.get("/rooms/:id", async (req, res) => {
      try {
        const { id } = req.params;

        const room = await Room.findById({ _id: id });
        // if (!room) return res.status(400).json({ err: "Этой комнаты не существует" });

        const actualPage = `/rooms/${id}`;
        const queryParams = {};
        req.body = { room: room ? JSON.stringify(room) : null };
        app.render(req, res, actualPage, queryParams);

        // res.json({ room });
      } catch (error) {
        console.log(error);
        return res.status(400).json({ err: "Ошибка" });
      }
    });
    server.delete("/rooms/:id", RoomController.delete);

    server.post("/transactionBy/:sessionIDParams", async (req, res) => {
      // const { lol } = req.body;
      // console.log(lol, "sessionID");
      const session = req.params.sessionIDParams;
      // const orderId = req.params.orderId;
      // console.log(orderId, "orderId");
      const [sessionID, orderId] = session.split("-");

      if (!sessionID || !orderId) {
        return res.status(400).json({ err: "Ошибка" });
      }

      // if (!sessionID === generateMD5(orderId + process.env.SECRET_KEY_SESSION)) {
      //   return res.status(400).json({ err: "Сессия истекла" });
      // }

      if (sessionID !== generateMD5(orderId + process.env.SECRET_KEY_SESSION)) {
        return res.status(400).json({ err: "Сессия истекла " });
      }

      const [order] = await Orders.find({ sessionToken: sessionID });

      if (!order) {
        return res.status(400).json({ err: "Сессия истекла" });
      }

      // console.log(order, "order");

      const endtime = Date.parse(order.sessionTime);
      const nowTime = Date.now();
      // let ts = Date.now();
      // console.log({ endtime, nowTime });
      // console.log({ ts, endtime, currentTime: order.sessionTime });

      res.json({
        message: { currentTime: nowTime > endtime ? false : endtime },
        paid: order.paid,
      });
    });

    server.post("/transactionByEnd/:sessionIDParams", async (req, res) => {
      const session = req.params.sessionIDParams;
      const [_, orderId] = session.split("-");

      if (!orderId) {
        return res.status(400).json({ err: "Ошибка" });
      }

      const rese = await Orders.findOneAndUpdate(
        { _id: orderId },
        {
          paid: true,
          dateOfPayment: new Date().toISOString(),
        }
      );

      // const [order] = await Orders.find({ sessionToken: sessionID });
      const [order] = await Orders.find({ _id: orderId });

      // if (!order) {
      //   return res.status(400).json({ err: "Сессия истекла" });
      // }
      console.log(order, "rese");
      res.json({
        message: "Оплачено",
        newOrder: order,
      });
    });

    // server.get("/transactionPaymentByOrder", (req, res) => {
    //   // billingToken null
    //   // facitilatorAccessToken afawf234ssdfsf
    //   // orderID 324324dsfs
    //   // payerID sdfsdfs
    //   // paymentID null
    //   res.json({
    //     message: "Здесь непосредственно нам нужно создать страницу с формой типа платежки",
    //   });
    // });

    // server.post("/order", (req, res) => {
    //   res.json({
    //     m: "t",
    //   });
    // });

    // all => GET / POST / PATCH / PUT
    server.all("*", (req, res) => handle(req, res)); // pages/api/обработчики от сюда берутся

    server.listen(port, (err) => {
      if (err) {
        console.log("Чёт упало))");
        throw err;
      }
      console.log(
        `> Сервер готов и запущен на http://localhost:${port} в режиме ${
          dev ? "development" : process.env.NODE_ENV
        }`
      );
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
