const Room = require("../../models/RoomModel");

class RoomController {
  async index(req, res) {
    try {
      const items = await Room.find({});

      res.status(200).json({ items });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ err: "Ошибка" });
    }
  }
  async create(req, res) {
    try {
      const { title, type } = req.body;
      const data = { title, type };
      console.log(title, type);
      if (!title || !type) {
        return res.status(400).json({ message: "Отсутствует заголовок или тип комнаты" });
      }
      const room = await Room.create({
        title,
        type,
      });

      res.json({
        msg: "Создание комнаты прошло успешно",
        room,
      });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ err: "Ошибка" });
    }
  }
  async show(req, res) {
    const { id } = req.params;

    const room = await Room.findById({ _id: id });
    if (!room) return res.status(400).json({ err: "Этой комнаты не существует" });

    res.json({ room });

    try {
    } catch (error) {
      console.log(error);
      return res.status(400).json({ err: "Ошибка" });
    }
  }
  async delete(req, res) {
    try {
      const { id } = req.params;
      await Room.findByIdAndDelete(id);

      res.json({ msg: "Deleted a Room." });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ err: "Ошибка" });
    }
  }
}
module.exports = new RoomController();
