import axios from "axios";

const baseUrl = process.env.BASE_URL;

const istanse = axios.create({
  baseURL: baseUrl,
  withCredentials: true,
});

istanse.interceptors.request((config) => {
  return config;
});

export default istanse;
