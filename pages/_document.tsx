import Document, { Html, Head, Main, NextScript } from "next/document";
import siteMetadata from "src/common/siteMetadata";
import React from "react";

const isProduction = process.env.NODE_ENV === "production";

console.log(isProduction, "Когда будет продакш, инжкетится скрипт с аналитикой");

export default class DocumentWrapper extends Document {
  render(): JSX.Element {
    return (
      <Html lang={siteMetadata.lang}>
        <Head>
          {/* <meta http-equiv="Content-Security-Policy" content="default-src *;
   img-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' *;
   style-src  'self' 'unsafe-inline' *"/> */}
          {/*<meta http-equiv="content-security-policy" content=" img-src * data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' 'nonce-a' blob: filesystem: *  "/>*/}
          {/* <meta http-equiv="Content-Security-Policy" content="default-src 'self'; img-src https://*; child-src 'none';"></meta> */}
          {/* {isProduction && (
            <>
              <script
                async
                src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GTAG}`}
              />
              <script
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{
                  __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.GTAG}', {
              page_path: window.location.pathname,
            });
          `,
                }}
              />
            </>
          )} */}
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          />
          {/* <script
            src={`https://www.paypal.com/sdk/js?client-id=${process.env.PAYPAL_CLIENT_ID}`}
          ></script> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
