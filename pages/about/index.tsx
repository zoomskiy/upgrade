import React from "react";

const about = () => {
  return (
    <div>
      about RESTURL_SPEAKERS {process.env.APITEST}
      <br />
      RESTURL_SESSIONS {process.env.RESTURL_SESSIONS}
    </div>
  );
};

export default about;
