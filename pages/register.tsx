import React, { useRef } from "react";

import { useForm } from "react-hook-form";

export default function Register() {
  const {
    //  register,
    handleSubmit,
    watch,
    // errors
  } = useForm();
  const passConfirm = useRef();
  passConfirm.current = watch("passwordConfirm");
  const onSubmit = (data: any) => console.log(data);

  // console.log(watch("firstName")); // эти часы похожи на наблюдателя за конкретным элементом dom // следите за входным значением, передавая его имя

  return (
    <div className="wrapper">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div>
          {/* <input
                type="text"
                name="firstName"
                ref={register({ required: true, maxLength: 20 })}
              /> */}

          {/* {errors.firstName && errors.firstName.type == "required" && (
                <span className="error">sorry name is required</span>
              )}
              {errors.firstName && errors.firstName.type == "maxLength" && (
                <span className="error">sorry the name must not exceed 20 caracters</span>
              )} */}
        </div>

        <div>
          {/*
              <input
                type="text"
                name="lastName"
                ref={register({ required: true, maxLength: 20 })}
              />


              {errors.lastName && errors.lastName.type == "maxLength" && (
                <span className="error">sorry lastname must not exceed 20 character</span>
              )}
              {errors.lastName && errors.lastName.type == "required" && (
                <span className="error">sorry lastname is required</span>
              )} */}
        </div>

        <div>
          {/* <input type="email" name="email" ref={register} />
              {errors.lastName && errors.lastName.type == "maxLength" && (
                <span className="error">sorry lastname must not exceed 20 character</span>
              )}
              {errors.lastName && errors.lastName.type == "required" && (
                <span className="error">sorry lastname is required</span>
              )} */}
        </div>

        <div>
          {/* <input
                type="password"
                name="password"
                ref={register({ required: true, minLength: 5 })}
              /> */}

          {/*
              {errors.password && errors.password.type == "maxLength" && (
                <span className="error">sorry password must exceed minimum 5 character</span>
              )}
              {errors.password && errors.password.type == "required" && (
                <span className="error">sorry password is required</span>
              )} */}
        </div>

        <div>
          {/* <input
                type="password"
                name="passwordConfirm"
                ref={register({
                  required: true,
                  validate: (value) => value === passConfirm.current,
                })}
              /> */}

          {/* {errors.passwordConfirm && errors.passwordConfirm.type == "validate" && (
                <span className="error">sorry password are not match</span>
              )}
              {errors.passwordConfirm && errors.passwordConfirm.type == "required" && (
                <span className="error">sorry password confirmation is required</span>
              )} */}
        </div>

        {/* <input type="submit" value="submit" style={{ fontSize: "20px" }} /> */}
      </form>
    </div>
  );
}
