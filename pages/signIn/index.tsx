import React, { useContext, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
// import { ErrorMessage } from "@hookform/error-message";
import Input from "components/Form/Input/Input/Input";
import Cookie from "js-cookie";
import { getData, postData } from "../../utils/fetchData";
import { ACTIONS } from "../../store/context/actions";
import { DataContext } from "../../store/context/globalState";
import { useRouter } from "next/router";
import Cookies from "nookies";

export type useFormTypes = {
  email: string;
  password: string;
  errors: string;
};

const LoginFormSchema = yup.object().shape({
  login: yup.string().required("Введите почту или логин"),
  password: yup.string().required("Введите пароль").min(6, "Минимальое кол-во символов 6"),
});

const SignIn = () => {
  const { state, dispatch } = useContext(DataContext);
  const router = useRouter();
  // if (state.auth.token) {
  //   router.push("/");
  // }
  const {
    register,
    formState: { errors },
    reset,
    handleSubmit,
  } = useForm<useFormTypes>({
    resolver: yupResolver(LoginFormSchema),
    reValidateMode: "onChange",
  });

  const onSubm: SubmitHandler<useFormTypes> = async (data) => {
    dispatch({ type: ACTIONS.NOTIFY, payload: { loading: true } });

    const res = await postData("auth/login", data);
    if (res.err) return dispatch({ type: ACTIONS.NOTIFY, payload: { error: res.err } });

    dispatch({
      type: ACTIONS.AUTH,
      payload: {
        token: res.access_token,
        user: res.user,
      },
    });

    Cookie.set("refreshtoken", res.refresh_token, {
      path: "api/auth/accessToken",
      expires: 7,
    });
    // Cookie.set("firstLogin", "true-1", {
    //   expires: 7,
    // });

    // localStorage.setItem("firstLogin", "true");
    router.push("/");
    dispatch({ type: ACTIONS.NOTIFY, payload: { success: res.msg } });
    reset();
  };

  // Сделали редирект на сервере
  // useEffect(() => {
  //   if (state.auth.token) {
  //     router.push("/");
  //   }
  // }, [state.auth]);

  return (
    <div style={{ width: "400px" }}>
      <Head>
        <title>Авторизация</title>
      </Head>
      <form autoComplete="off" onSubmit={handleSubmit(onSubm)}>
        <Input
          type="text"
          placeholder="почта или логин"
          label="почта или логин"
          name="login"
          errorMessage={errors.login}
          ref={register}
        />
        <Input
          type="password"
          label="password"
          placeholder="password"
          name="password"
          errorMessage={errors.password}
          ref={register}
        />
        <div>
          <input type="submit" />
        </div>

        <div style={{ padding: "10px 0" }}>
          <Link href="/forgot_password">
            <a>Забыли пароль?</a>
          </Link>
          <Link href="/signUp">
            <a>Регистрация</a>
          </Link>
        </div>
      </form>
    </div>
  );
};

export async function getServerSideProps({ req, res }) {
  if (!req.headers.cookie) {
    return {
      props: {},
    };
  }

  const response = await getData("auth/accessToken", req.headers.cookie.split("=")[1]);

  // // 2 варианта редиректа
  // if (!response.err) {
  //   res.writeHead(301, { Location: "/" });
  //   res.end();
  // }
  if (!response.err) {
    return {
      props: {},
      redirect: {
        // permament: false,
        destination: "/",
      },
    };
  }

  return {
    props: {}, // will be passed to the page component as props
  };
}

export default SignIn;
