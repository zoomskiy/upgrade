import Products from "../../../models/ProductModel";

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      // await getProduct(req, res);
      break;
    case "PUT":
      // await updateProduct(req, res);
      break;
    case "DELETE":
      // await deleteProduct(req, res);
      break;
  }
};

const getProduct = async (req, res) => {
  try {
    const { id } = req.query;

    res.json({ msg: "" });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};

const updateProduct = async (req, res) => {
  try {
    const { id } = req.query;
    res.json({ msg: "" });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};

const deleteProduct = async (req, res) => {
  try {
    const { id } = req.query;

    res.json({ msg: "" });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};
