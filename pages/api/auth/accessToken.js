import jwt from "jsonwebtoken";
// import connectDB from "../../../utils/ConnectDB";
import { createAccessToken } from "../../../utils/generateToken";
import Users from "../../../models/UserModal";

// connectDB()

export default async (req, res) => {
  try {
    const rf_token = req.cookies.refreshtoken;
    const rf_token_for_backend = req.headers.authorization;
    // console.log(rf_token_for_backend, "req.cookies");
    // console.log(req, "req.cookies");

    // console.log({ rf_token: rf_token, backend: rf_token_for_backend });
    if (!rf_token_for_backend) {
      return res.status(400).json({ err: "Пожалуйста залогиньтесь!" });
    }
    // if (!rf_token || !rf_token_for_backend) {
    //   return res.status(400).json({ err: "Пожалуйста залогиньтесь!" });
    // }

    // const result = jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET);
    const result2 = jwt.verify(rf_token_for_backend, process.env.REFRESH_TOKEN_SECRET);
    if (!result2) return res.status(400).json({ err: "Ваш токен некорректный или истёк." });
    // if (!result || !result2)
    //   return res.status(400).json({ err: "Ваш токен некорректный или истёк." });

    // const user = await Users.findById(result.id).exec();
    const user2 = await Users.findById(result2.id).exec();

    if (!user2) return res.status(400).json({ err: "Пользователь не существует!" });
    // if (!user || !user2) return res.status(400).json({ err: "Пользователь не существует!" });

    const access_token = createAccessToken({ id: user2._id });
    // const access_token = createAccessToken({ id: user ? user._id : user2_id });

    res.json({
      access_token,
      user: {
        username: user2.username,
        fullname: user2.fullname,
        email: user2.email,
        role: user2.role,
        avatar: user2.avatar,
        root: user2.root,
      },
    });
    // res.json({
    //   access_token,
    //   user: {
    //     username: user ? user.username : user2.username,
    //     fullname: user ? user.fullname : user2.fullname,
    //     email: user ? user.email : user2.email,
    //     role: user ? user.role : user2.role,
    //     avatar: user ? user.avatar : user2.avatar,
    //     root: user ? user.root : user2.root,
    //   },
    // });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};
