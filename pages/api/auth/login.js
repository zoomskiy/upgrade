import { generateMD5 } from "utils/generateHash";
import Users from "../../../models/UserModal";
// import bcrypt from "bcrypt";
// import connectDB from "../../../utils/ConnectDB";
import { createAccessToken, createRefreshToken } from "../../../utils/generateToken";

// connectDB()

export default async (req, res) => {
  switch (req.method) {
    case "POST":
      await login(req, res);
      break;
  }
};

const login = async (req, res) => {
  try {
    const { login, password } = req.body;

    //TODO: зарефакторить действия в отдельную функцию (Service) или ещё чего
    const user = await Users.findOne({
      $or: [{ email: login }, { username: login }],
    }).exec();

    //TODO: Вынести ошибки в константы и коды
    if (!user) return res.status(400).json({ err: "Этого аккаунта не существует!" });

    // const isMatch = await bcrypt.compare(password, user.password);
    // const isMatch = password === user.password;
    const isMatch = user.password === generateMD5(password + process.env.SECRET_KEY);
    if (!isMatch) return res.status(400).json({ err: "Некорректаные данные" });

    const access_token = createAccessToken({ id: user._id });
    const refresh_token = createRefreshToken({ id: user._id });

    res.json({
      msg: "Login Success!",
      refresh_token,
      access_token,
      user: {
        id: user._id,
        username: user.username,
        fullname: user.fullname,
        email: user.email,
        role: user.role,
        avatar: user.avatar,
        root: user.root,
      },
    });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};
