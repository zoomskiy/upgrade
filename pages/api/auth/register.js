// import bcrypt from "bcrypt";
import { generateMD5 } from "utils/generateHash";
import Users from "../../../models/UserModal";
import valid from "../../../utils/valid";
// import connectDB from "../../../utils/ConnectDB";

// connectDB();

export default async (req, res) => {
  switch (req.method) {
    case "POST":
      await register(req, res);
      break;
  }
};

const register = async (req, res) => {
  try {
    const { username, email, password, password2, gender, fullname } = req.body;

    const errMsg = valid(username, email, password, password2, gender, fullname);

    if (errMsg) return res.status(400).json({ err: errMsg });

    let newUsername = username.toLowerCase().replace(/ /g, "");

    const user = await Users.findOne({
      $or: [{ email }, { username: newUsername }],
    }).exec();

    if (user) {
      res.status(400).json({ err: "Ошибка !ЭТО ТЕСТОВАЯ ВЕРСИЯ" });
    }
    const passwordHash = generateMD5(password + process.env.SECRET_KEY);
    // const passwordHash = await bcrypt.hash(password, 12);
    const newUser = await Users.create({
      username: newUsername,
      email,
      password: passwordHash,
      gender,
      fullname,
    });

    res.json({
      msg: "Регистрация прошла успешна",
      data: newUser,
    });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};
