import Orders from "models/OrderModel";
import Products from "models/ProductModel";
import { generateMD5 } from "utils/generateHash";
import auth from "../../../middleware/auth";

const sold = async (id, quantity, oldInStock, oldSold) => {
  await Products.findOneAndUpdate(
    { _id: id },
    {
      inStock: oldInStock - quantity,
      sold: quantity + oldSold,
    }
  );
};

export default async (req, res) => {
  switch (req.method) {
    case "POST":
      await createOrder(req, res);
      break;
    case "GET":
      await getOrders(req, res);
      break;
  }
};

const getOrders = async (req, res) => {
  try {
    const result = await auth(req, res);

    let orders;
    // console.log(result, 'koo')
    if (result.role !== "admin") {
      orders = await Orders.find({ user: result.id }).populate("User");
    } else {
      orders = await Orders.find().populate("User");
    }

    res.json({ orders });
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};

const createOrder = async (req, res) => {
  try {
    const token = req.headers.authorization;
    let result;
    if (token) {
      result = await auth(req, res);
      const {
        address,
        email,
        firstName,
        lastName,
        phone,
        shippingDec = "",
        paymentType,
        total,
        cart,
      } = req.body;

      const newOrder = await Orders.create({
        user: result.id,
        address,
        firstName,
        lastName,
        shippingDec,
        paymentType,
        email,
        phone,
        cart,
        total,
        sessionTime: new Date(new Date().getTime() + 5 * 60000),
        sessionToken: "",
      });

      const newOrderCurrent = await Orders.findOneAndUpdate(
        {
          _id: newOrder._id,
          // user: result.id,
        },
        {
          // sessionToken: "123",
          sessionToken: generateMD5(newOrder._id + process.env.SECRET_KEY_SESSION),
        },
        { new: true }
      );

      cart.filter((item) => {
        return sold(item._id, item.quantity, item.inStock, item.sold);
      });
      // console.log(newOrderCurrent, "newOrderCurrent");
      res.json({
        msg: "Order success! We will contact you to confirm the order.",
        newOrder: newOrderCurrent,
        sessionToken: newOrderCurrent.sessionToken,
        // sessionTime: newOrder.sessionTime,
      });
    } else {
      return res.status(400).json({ err: "Ошибка авторизации" });
    }
  } catch (err) {
    return res.status(500).json({ err: err.message });
  }
};
