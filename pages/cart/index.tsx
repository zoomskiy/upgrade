import Head from "next/head";
import { useContext, useState, useEffect } from "react";
import Link from "next/link";
import { getData } from "../../utils/fetchData";
import { DataContext } from "../../store/context/globalState";
import { ACTIONS } from "../../store/context/actions";
import CartItem2 from "../../components/product/CartItem2";
import PaymentPage from "components/PaypalBtn/PaymentPage";

const Cart = () => {
  const { state, dispatch } = useContext(DataContext);
  const { cart, auth, orders } = state;

  const [total, setTotal] = useState(0);

  const [updateCartCallback, setUpdateCartCallback] = useState(false);

  useEffect(() => {
    const getTotal = () => {
      const res = cart.reduce((prev, item) => {
        return prev + item.price * item.quantity;
      }, 0);

      setTotal(res);
    };

    getTotal();
  }, [cart]);

  useEffect(() => {
    const cartLocal = JSON.parse(localStorage.getItem("__next__cart01__devat"));
    // console.log(cartLocal);
    if (cartLocal && cartLocal.length > 0) {
      let newArr = [];
      const updateCart = async () => {
        for (const item of cartLocal) {
          const res = await getData(`products/${item._id}`);
          const { _id, title, images, price, inStock, sold } = res.product;
          if (inStock > 0) {
            newArr.push({
              _id,
              title,
              images,
              price,
              inStock,
              sold,
              quantity: item.quantity > inStock ? 1 : item.quantity,
            });
          }
        }

        dispatch({ type: ACTIONS.ADD_CART, payload: newArr });
      };

      updateCart();
    }
  }, [updateCartCallback]);

  if (cart.length === 0) return <div>В корзине пусто</div>;
  // return <img className="img-responsive w-100" src="/empty_cart.jpg" alt="not empty"/>

  return (
    <div className="row mx-auto">
      <Head>
        <title>Cart Page</title>
      </Head>

      <div className="col-md-8 text-secondary table-responsive my-3">
        <h2 className="text-uppercase">Shopping Cart</h2>

        {/* <tbody>
              {
                cart.map(item => (
                  <CartItem key={item._id} item={item} dispatch={dispatch} cart={cart} />
                ))
              }
            </tbody> */}
        <CartItem2 cart={cart} dispatch={dispatch} />
      </div>

      <div className="col-md-4 my-3 text-right text-uppercase text-secondary">
        <h3>
          Общая стоимость: <span className="text-danger">${total}</span>
        </h3>
        {/* <div>Перейти к оформлению</div> */}
        {/* <Link href={auth.user ? "#!" : "/signIn"}> */}
        {auth.user ? (
          <PaymentPage setUpdateCartCallback={setUpdateCartCallback} total={total} />
        ) : (
          <Link href="/signIn">
            <a>Оформление заказа после авторизации</a>
          </Link>
        )}
      </div>
    </div>
  );
};

export default Cart;
