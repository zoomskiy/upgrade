// import Helmet from "react-helmet";
// import Link from "next/link";
// import style from "styles/index.module"
import Head from "next/head";
import { getData } from "../utils/fetchData";
import { useState } from "react";
import ProductItem from "../components/product/ProductItem";
import { useSelector } from "react-redux";

const Home = ({ products, result }: any) => {
  const [productsState, setProducts] = useState(products);
  const state = useSelector((state) => state);
  console.log(state);

  // const handleCheck = (id) => {
  //   products.forEach(product => {
  //     if(product._id === id) product.checked = !product.checked
  //   })
  //   setProducts([...products])
  // }

  return (
    <div>
      <Head>
        <title>Homepage</title>
      </Head>
      <div style={{ padding: "50px" }}>
        {productsState.length === 0 ? (
          <h2>Нет продуктов</h2>
        ) : (
          productsState.map((product: any) => (
            <ProductItem
              key={product._id}
              product={product}
              //  handleCheck={handleCheck}
            />
          ))
        )}
      </div>
    </div>
  );
};

export default Home;

export async function getServerSideProps() {
  const response = await getData("products");
  return { props: { products: response.products, result: response.result } };
}

// export async function getServerSideProps(context) {
//     return {props: {session: await getSession(context)}}
// }

// export default class INDEX_PAGE extends React.Component {
//   static propTypes = {};
//   static defaultProps = {};
//   render() {
//     return (
//       <article></article>
//         <Helmet title="Home" />
//         <section>
//           <h1 className={style.test}>Hello</h1>
//           <p>NextJS, Node/Express</p>
//           <Link href="/about">
//             <a src="#" title="About Nexts">
//               About
//             </a>
//           </Link>
//         </section>
//       </article>
//     );
//   }
// }
// import { getSession } from "next-auth/client";
// import Link from "next/link";
// import React from "react";
// import Wrapper from "../components/Layout/Wrapper";

// export default function Index({session}){
//   return (
//       <Wrapper session={session}>
//           {
//           }
//         <h1> Welcome to NextJS Base. </h1>
//         <p>
//             This is a starter template with authentication, scss, and other essentials already baked in,
//             so you don't have to waste your time setting up a full stack app.
//             {
//                 session ? <Link href="/dashboard"> Access your dashboard </Link> : <Link href="/auth/login"> Login </Link>
//             }
//         </p>
//       </Wrapper>
//   )
// }

// export async function getServerSideProps(context) {
//     return {props: {session: await getSession(context)}}
// }
