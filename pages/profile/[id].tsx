import { useRouter } from "next/router";
import React from "react";

const profile = () => {
  const router = useRouter();
  const { id } = router.query;
  return <div>profile {id}</div>;
};

export default profile;
