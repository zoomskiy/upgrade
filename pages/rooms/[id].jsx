import React from "react";

const Room = ({ room }) => {
  console.log(room);
  return (
    <div>
      <div>
        {room ? (
          <div>
            {Object.keys(room).map((item, key) => {
              return (
                <div style={{ backgroundColor: "#ccc" }} key={key}>
                  <div>
                    {item} - {room[item]}
                  </div>
                </div>
              );
            })}
          </div>
        ) : (
          <div>Комнаты не существует</div>
        )}
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { room } = context.req.body;

  return {
    props: {
      room: JSON.parse(room),
    },
  };
}
export default Room;
