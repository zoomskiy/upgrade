import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Link from "next/link";

const Room = ({ rooms }) => {
  const [value, setvalue] = useState("");
  const router = useRouter();
  const handleCreate = async () => {
    try {
      if (!value) {
        alert("Имя комнаты не введено!");
        return;
      }

      const res = await axios.post("http://localhost:1333/rooms", {
        title: value,
        type: "base",
      });

      if (res.message) {
        alert(res.message);
        setvalue("");
        return;
      }

      router.push(`/rooms/${res.data.room._id}`);
      setvalue("");
    } catch (error) {
      alert(error);
      setvalue("");
    }
  };
  return (
    <div style={{ padding: "50px" }}>
      Здесь будут комнаты
      <div>
        {rooms.length ? (
          rooms.map(({ title, type, listenersCount, speakers, _id }, idx) => {
            return (
              <div style={{ backgroundColor: "#ccc", margin: "10px" }} key={idx}>
                <div>{title}</div>
                <div>{type}</div>
                <div>Кол-во слушателей: {listenersCount}</div>
                <div>Speakers: {speakers || "Еще нет спикеров"}</div>
                <div>
                  <Link href={`/rooms/${_id}`}>
                    <a>Открыть комнату</a>
                  </Link>
                </div>
              </div>
            );
          })
        ) : (
          <div>Нет активных комнат</div>
        )}
      </div>
      <div style={{ padding: "50px" }}>
        <div>
          Название комнаты:{" "}
          <input type="text" value={value} onChange={(e) => setvalue(e.target.value)} />
        </div>
        <button onClick={handleCreate} disabled={!Boolean(value)}>
          СОЗДАТЬ КОМНАТУ
        </button>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { rooms } = context.req.body;

  return {
    props: {
      rooms: JSON.parse(rooms),
    },
  };
}
export default Room;
