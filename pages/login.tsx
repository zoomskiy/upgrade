import React, { useState } from "react";

import Head from "next/head";
import Link from "next/link";
import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { ErrorMessage } from "@hookform/error-message";

// const schema = yup.object().shape({
//   email: yup
//     .string()
//     .email("Email should have correct format")
//     .required("Email is a required field"),
// });

// const schema = yup.object().shape({
//   firstName:yup.string().matches(/^([^0-9]*)$/,"First field should not contain numbers").required("first name is a required field"),
//   lastName:yup.string().matches(/^([^0-9]*)$/,"last field should not contain numbers").required("last name is a required field")
// })

// const schema = yup.object().shape({
//   firstName: yup.string().required("First name is required"),
//   lastName: yup.string().required("Last name is required"),
//   password: yup.string().required("Password is required").min(8, "Password must be at least 8 characters"),
//   emailAddress: yup
//     .string()
//     .required("Email address is required")
//     .email("Invalid email address"),
//     confirmPassword: yup.string()
//     .oneOf([yup.ref("password", "")], "Passwords must match").required("Please confirm password")
// });

type FormInputs = {
  // email: string;
  password: string;
  // errors: string;
};

// const validationSchema = yup.object().shape({
//   fullName: yup.string().required()
// })
const LoginFormSchema = yup.object().shape({
  // email: yup.string().email("Неверная почта").required("Введите почту"),
  password: yup.string().required("Введите пароль"),
});

export const inputField = (placeholder: string, type: any) => {
  if (type === "email") {
    return <input placeholder={placeholder} />;
  }
  if (type === "password") {
    return <input type="password" placeholder={placeholder} />;
  }
  return "";
};

const LoginPage = () => {
  // let [loading, setLoading] = useState(false)
  // const [showPassword, setShowPassword] = useState(false);
  const {
    register,
    formState: { errors },
    // errors,
    reset,
    // watch,
    // control,
    // setValue,
    // formState,
    handleSubmit,
  } = useForm({
    mode: "onSubmit",
    // mode: "onBlur",
    // mode: 'onTouched',
    // mode: "onChange",
    resolver: yupResolver(LoginFormSchema),
  });

  const handleLogin: SubmitHandler<FormInputs> = () => {
    reset();
  };

  // const isShowPassword = () => {
  //   console.log("s");
  //   setShowPassword((prev) => setShowPassword(prev));
  // };
  return (
    <div>
      <Head>
        <title>Lol</title>
      </Head>
      <form autoComplete="off" onSubmit={handleSubmit(handleLogin)}>
        <div>
          <input
            className="form-input"
            type="password"
            // type={showPassword ? "text" : "password"}
            name="password"
            // ref={register("password")}
            autoComplete="new-password"
            placeholder="password"
            id="password"
            // ref={register({ required: "required" })}

            // render={({ onChange, onBlur, value }) => (
          />
          {/* <span onClick={isShowPassword} aria-hidden="true">
            {showPassword ? "Показать пароль" : "Скрыть пароль"}
          </span> */}
        </div>

        {/* <input type="text" {...register("email")} /> */}

        <button type="submit" className="form-button">
          Логин
        </button>
        <Link href="/forgot_password">
          <a>Забыли пароль?</a>
        </Link>
        <Link href="/signUp">
          <a>Регистрация</a>
        </Link>

        {/* <input
          type="text"
          name="fullName"
          ref={register()}
        />
        {errors.fullName && (
          <p className="error"> {errors.fullName.message} </p>
        )} */}

        <div>
          {errors.email?.message && errors.email.message}
          {errors.password?.message && errors.password.message}
        </div>

        {/* <button type="button" onClick={handleSubmit(onSubmit)} disabled={formState.isSubmitting}>
          {loading ? "loading" : "Sign Up"}
        </button> */}
      </form>
    </div>
  );
};

export default LoginPage;
