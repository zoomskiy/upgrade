import { useRouter } from "next/router";
import React, { useEffect, useState, useRef } from "react";
const baseUrl = process.env.BASE_URL;

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  return {
    total: t,
    minutes,
    seconds,
  };
}

const Transaction = ({ sessionID, isSessionID }) => {
  // const router = useRouter();
  const [newOrder, setnewOrder] = useState([]);
  const [loading, setloading] = useState(true);
  const [loading2, setloading2] = useState(true);
  const [error, seterror] = useState("");
  const [minutesSpan, setminutesSpan] = useState("");
  const [secondsSpan, setsecondsSpan] = useState("");
  const [timeinterval, settimeinterval] = useState(null);
  const [succes, setsucces] = useState("");
  const [sekund, setsekund] = useState(5);
  const ref = useRef();
  function initializeClock(endtime) {
    function updateClock() {
      var t = getTimeRemaining(endtime);

      setminutesSpan(("0" + t.minutes).slice(-2));
      setsecondsSpan(("0" + t.seconds).slice(-2));

      if (t.minutes <= 0 && t.seconds <= 0) {
        clearInterval(ref.current);
        seterror("Время оформления заказа истекло");
        return;
      }
      // if (t.minutes <= 0 && t.seconds <= 0) {
      //   console.log('FFALSE')
      //   clearInterval(timeinterval);
      // }
    }

    updateClock();
    const timeintervalID = setInterval(updateClock, 1000);
    ref.current = timeintervalID;

    setloading2(false);
  }

  useEffect(() => {
    const checkSession = async () => {
      try {
        const res = await fetch(`http://localhost:1333/transactionBy/${sessionID}`, {
          method: "POST",
          // body: "123",
          // body: JSON.stringify({ lol: "3" }),
        });
        const data = await res.json();
        if (data.err) {
          seterror(data.err);
          return;
        }

        if (data.paid === true) {
          setsucces("Уже оплачено");
          return;
        }

        // new Date(new Date().getTime() + 5 * 60000)
        const currentTime = data.message.currentTime;
        if (!currentTime) {
          seterror("Сессия истекла");
          return;
        }

        initializeClock(new Date(currentTime).toISOString());

        setloading(false);

        // window.opener.postMessage("xui", "*");
        // window.close();
      } catch (errors) {
        setminutesSpan("");
        setsecondsSpan("");
        seterror(errors.message);
      }
    };
    checkSession();
    return () => {
      clearInterval(timeinterval);
    };
  }, []);

  const handlefinish = async () => {
    try {
      const res = await fetch(`http://localhost:1333/transactionByEnd/${sessionID}`, {
        method: "POST",
      });
      const data = await res.json();
      if (data.err) {
        seterror(data.err);
        return;
      }
      setsucces(data.message);
      setnewOrder(data.newOrder);
      const [_, orderId] = sessionID.split("-");
      window.opener.postMessage(
        JSON.stringify({ end: `successPayment`, orderId, orderProduct: data.newOrder }),
        "*"
      );

      // setTimeout(() => {
      //   const [_, orderId] = sessionID.split('-')
      //   // window.opener.postMessage(`successPayment-${orderId}`, "*");
      //   // window.opener.postMessage('123123', "*");
      //   window.opener.postMessage(JSON.stringify({end: `successPayment`, orderId, orderProduct: data.newOrder}), "*");
      //   // window.close();
      // }, 5000);
    } catch (errors) {
      seterror(errors.message);
    }
  };

  if (!isSessionID) {
    return <div>Ошибка с транзакцией</div>;
  }

  if (succes) {
    return (
      <div>
        {succes}

        {/* <div
          onClick={() => {
            const [_, orderId] = sessionID.split('-')

            window.opener.postMessage(JSON.stringify({end:`successPayment`, orderId,orderProduct: newOrder}), "*");
            // window.close();
          }}
          style={{ color: "red", fontWeight: "700" }}
        >
          Перейти обратно на сайт
        </div> */}
        <div>Оплачено!</div>

        <div style={{ color: "red" }} onClick={() => window.close()}>
          Закрыть
        </div>
        {/* <div>Вы автоматически вернетесь на сайт в течение {sekund} секунд</div> */}
      </div>
    );
  }

  if (error) {
    return <div>{error}</div>;
  }

  if (loading) {
    return <div>Loading</div>;
  }

  if (loading2) {
    return <div>Loading</div>;
  }

  return (
    <div>
      <span>{minutesSpan}</span> :<span>{secondsSpan}</span>
      <div onClick={handlefinish}>Оплатить</div>
    </div>
  );
};

export async function getServerSideProps({ params: { sessionID } }) {
  let isSessionID = true;
  if (!sessionID) {
    isSessionID = false;
  }
  return {
    props: { isSessionID, sessionID },
  };
}

export default Transaction;
