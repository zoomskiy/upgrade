import React, { useContext, useEffect } from "react";
import Head from "next/head";
import Link from "next/link";
import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Input from "components/Form/Input/Input/Input";
import MySelect from "components/Form/Select/Select";
import { DataContext, useNotification } from "../../store/context/globalState";
import { ACTIONS } from "../../store/context/actions";
import { getData, postData } from "../../utils/fetchData";
import { useRouter } from "next/router";

export type useFormTypes = {
  email: string;
  password: string;
  errors: string;
  username: string;
  fullname: string;
  password2: string;
  gender: string;
};

const RegisterFormSchema = yup.object().shape({
  username: yup.string().required("Введите обязательно ник"),
  fullname: yup.string().required("Введите полное имя"),
  email: yup.string().email("Неверная почта").required("Введите почту"),
  password: yup.string().min(6, "​Минимальная длина пароля 6 символов").required(),
  password2: yup.string().oneOf([yup.ref("password")], "Пароли не соответствуют"),
  gender: yup.string().required("Обязательное поле"),
});

const signUp = () => {
  const { state, dispatch } = useContext(DataContext);
  const dispatchNotification = useNotification();
  const router = useRouter();
  // if (state.auth.token) {
  //   router.push("/");
  // }
  const {
    register,
    formState: { errors },
    reset,
    handleSubmit,
    setValue,
    watch,
  } = useForm<useFormTypes>({
    resolver: yupResolver(RegisterFormSchema),
    reValidateMode: "onChange",
  });

  const watchEmail = watch("email");
  const watchPassword = watch("password");
  const watchUsername = watch("username");
  const watchFullname = watch("fullname");
  const watchPassword2 = watch("password2");
  const watchGender = watch("gender");

  const onSubm: SubmitHandler<useFormTypes> = async (data) => {
    dispatch({ type: ACTIONS.NOTIFY, payload: { loading: true } });
    const res = await postData("auth/register", data);
    // if (res.err) return dispatch({ type: ACTIONS.NOTIFY, payload: { error: res.err } });
    if (res.err) {
      dispatchNotification({
        type: "ERROR",
        message: res.err,
        title: "ERROR req",
      });
      dispatch({ type: ACTIONS.NOTIFY, payload: { loading: false } });
      return;
    }
    dispatchNotification({
      type: "SUCCESS",
      message: res.msg,
      title: "SUCCESS req",
    });
    // dispatch({ type: ACTIONS.NOTIFY, payload: { success: res.msg } });
    dispatch({ type: ACTIONS.NOTIFY, payload: { loading: false } });
    router.push("/signIn");
    reset();
  };

  // Сделали редирект на сервере
  // useEffect(() => {
  //   if (state.auth.token) {
  //     router.push("/");
  //   }
  // }, [state.auth]);

  return (
    <div className="contaner__formUp">
      <Head>
        <title>Регистрация</title>
      </Head>
      <form onSubmit={handleSubmit(onSubm)} className="formUp">
        <Input
          type="text"
          placeholder="Имя"
          label="Полное имя"
          name="fullname"
          errorMessage={errors.fullname}
          ref={register}
        />
        <Input
          type="text"
          placeholder="Логин"
          label="Логин"
          name="username"
          errorMessage={errors.username}
          ref={register}
          onChange={(e) => setValue("username", e.target.value.toLowerCase().replace(/ /g, ""))}
        />
        <Input
          type="text"
          placeholder="Почта"
          label="Почта"
          name="email"
          errorMessage={errors.email}
          ref={register}
        />
        <Input
          type="password"
          placeholder="Пароль"
          label="Пароль"
          name="password"
          errorMessage={errors.password}
          ref={register}
        />
        <Input
          type="password"
          placeholder="Подтвердите пароль"
          label="Подтвердите пароль"
          name="password2"
          errorMessage={errors.password2}
          ref={register}
        />

        <MySelect
          labelTitle="Пол"
          id="gender"
          name="gender"
          errors={errors.gender}
          options={[
            { value: "", label: "Выбрать" },
            { value: "female", label: "Женский" },
            { value: "male", label: "Мужской" },
          ]}
          ref={register}
        />
        <div style={{ textAlign: "center" }}>
          <button
            className="formUp__button"
            disabled={
              !(
                watchEmail &&
                watchPassword &&
                watchUsername &&
                watchFullname &&
                watchPassword2 &&
                watchGender
              )
            }
            type="submit"
          >
            {state.notify.loading ? "Выполняется регистрация" : " Зарегистрироваться"}
          </button>
        </div>
        <div style={{ padding: "10px 0" }}>
          <p>
            Уже есть почта?
            <Link href="/signIn">Войти</Link>
          </p>
        </div>
      </form>
    </div>
  );
};

export async function getServerSideProps({ req, res }) {
  if (!req.headers.cookie) {
    return {
      props: {},
    };
  }

  const response = await getData("auth/accessToken", req.headers.cookie.split("=")[1]);

  // // 2 варианта редиректа
  // if (!response.err) {
  //   res.writeHead(301, { Location: "/" });
  //   res.end();
  // }
  if (!response.err) {
    return {
      props: {},
      redirect: {
        // permament: false,
        destination: "/",
      },
    };
  }

  return {
    props: {}, // will be passed to the page component as props
  };
}

export default signUp;
