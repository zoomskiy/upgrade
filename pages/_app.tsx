import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import "styles/global";
import { useLocalStorageState } from "src/hooks/local-storage";
import { Layout } from "../components/Layouts/Layout";
import { DataProvider } from "../store/context/globalState";
import { wrapper } from "store/redux/work";
import Transaction from "./transactionPaymentOrder/[sessionID]";
import Cookies from "nookies";
import { getData } from "utils/fetchData";

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  const [darkMode, setDarkMode] = useLocalStorageState({
    key: "is-dark-mode",
    defaultValue: false,
  });
  // console.log(
  //   darkMode,
  //   setDarkMode,
  //   "Здесь непосредственно будет логика с прокидываем контекста по всему проекту и управлением состояние темы."
  // );

  if (Component === Transaction) {
    return <Component {...pageProps} />;
  }
  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"
        />
        {/* <meta
        httpEquiv="content-security-policy" content=" img-src 'self' data: blob:; script-src 'self' 'unsafe-inline' 'unsafe-eval' 'nonce-a' blob: filesystem: *  "/> */}
      </Head>
      <DataProvider token={pageProps.token} user={pageProps.user}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </DataProvider>
    </>
  );
};

App.getInitialProps = async (appctx) => {
  const cookies = Cookies.get(appctx.ctx);
  const res = await getData("auth/accessToken", cookies.refreshtoken);

  if (res.err) {
    return {
      pageProps: {
        token: null,
        user: null,
      },
    };
  }

  return {
    pageProps: {
      token: res.access_token,
      user: res.user,
    },
  };
};

export default wrapper.withRedux(App);
