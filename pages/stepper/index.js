import StepperPage from "components/Stepper/StepperPage";
import React from "react";

const Stepper = () => {
  return (
    <div style={{ padding: "50px" }}>
      <StepperPage />
    </div>
  );
};

export default Stepper;
