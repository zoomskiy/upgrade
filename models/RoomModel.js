const mongoose = require("mongoose");

const RoomSchema = new mongoose.Schema(
  {
    title: String,
    speakers: {
      type: String,
      default: null,
    },
    listenersCount: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

let Dataset = mongoose.models.room || mongoose.model("room", RoomSchema);

module.exports = Dataset;
// export default Dataset;
