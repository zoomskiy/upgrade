const mongoose = require("mongoose");
// const { isEmail } = require("validator");

const UserSchema = new mongoose.Schema(
  {
    email: {
      unique: true,
      required: [true, "email address is required"],
      trim: true,
      type: String,
      // validate: [isEmail, "Invalid email"],
    },
    fullname: {
      type: String,
      required: [true, "Fullname is required"],
      trim: true,
      maxlength: 25,
    },
    role: {
      type: String,
      default: "user",
    },
    root: {
      type: Boolean,
      default: false,
    },
    avatar: {
      type: String,
      default: "",
    },
    username: {
      unique: true,
      required: true,
      type: String,
      trim: true,
      maxlength: 25,
    },
    password: {
      type: String,
      required: [true, "Password is required"],
    },
    confirmHash: {
      required: false,
      type: String,
    },
    confirmed: {
      type: Boolean,
      default: false,
    },
    location: String,
    admin: {
      type: Boolean,
      default: false,
    },
    gender: {
      type: String,
      default: "male",
    },
    mobile: {
      type: String,
      default: "",
    },
    address: {
      type: String,
      default: "",
    },
    story: {
      type: String,
      default: "",
      maxlength: 200,
    },
    website: {
      type: String,
    },
    followers: [{ type: mongoose.Types.ObjectId, ref: "User" }],
    following: [{ type: mongoose.Types.ObjectId, ref: "User" }],
    about: String,
    last_seen: {
      type: Date,
      default: new Date(),
    },
  },
  {
    timestamps: true,
  }
);

UserSchema.set("toJSON", {
  transform: function (_, obj) {
    delete obj.password;
    return obj;
  },
});

// let Dataset = mongoose.models.user || mongoose.model('User', UserSchema)
// const Dataset = mongoose.model("User", UserSchema);

// export default Dataset;

const Dataset = mongoose.models.User || mongoose.model("User", UserSchema);

// export default Dataset;
module.exports = Dataset;
