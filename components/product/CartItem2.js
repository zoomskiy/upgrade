import React, { useCallback, useState } from "react";
import CartItem from "./CartItem";
import Modal from "./Modal2";

const CartItem2 = ({ cart, dispatch }) => {
  const [visibleModal, setVisibleModal] = useState();

  const handleCloseModal = useCallback(() => {
    setVisibleModal(undefined);
  }, []);

  const handleClickOpenSignIn = () => {
    setVisibleModal("deleteCartItem");
  };
  return (
    <div>
      <table className="table my-3">
        <tbody>
          {cart.map((item) => (
            <CartItem
              key={item._id}
              item={item}
              dispatch={dispatch}
              cart={cart}
              handleClickOpenSignIn={handleClickOpenSignIn}
            />
          ))}
        </tbody>
      </table>
      <Modal displayModal={visibleModal === "deleteCartItem"} closeModal={handleCloseModal} />
    </div>
  );
};

export default CartItem2;
