import React, { memo, useCallback, useContext } from "react";
import { DataContext } from "../../store/context/globalState";
import { useRouter } from "next/router";
import { deleteItem } from "../../store/context/actions";
import { deleteData } from "../../utils/fetchData";

const Modal = memo(({ displayModal, closeModal, children }) => {
  const { state, dispatch } = useContext(DataContext);
  const { modal, auth } = state;

  const router = useRouter();

  const deleteUser = (item) => {
    dispatch(deleteItem(item.data, item.id, item.type));

    deleteData(`user/${item.id}`, auth.token).then((res) => {
      if (res.err) return dispatch({ type: "NOTIFY", payload: { error: res.err } });
      return dispatch({ type: "NOTIFY", payload: { success: res.msg } });
    });
  };

  const deleteCategories = (item) => {
    deleteData(`categories/${item.id}`, auth.token).then((res) => {
      if (res.err) return dispatch({ type: "NOTIFY", payload: { error: res.err } });

      dispatch(deleteItem(item.data, item.id, item.type));
      return dispatch({ type: "NOTIFY", payload: { success: res.msg } });
    });
  };

  const deleteProduct = (item) => {
    dispatch({ type: "NOTIFY", payload: { loading: true } });
    deleteData(`product/${item.id}`, auth.token).then((res) => {
      if (res.err) return dispatch({ type: "NOTIFY", payload: { error: res.err } });
      dispatch({ type: "NOTIFY", payload: { success: res.msg } });
      return router.push("/");
    });
  };

  const handleSubmit = () => {
    if (modal.length !== 0) {
      for (const item of modal) {
        if (item.type === "ADD_CART") {
          dispatch(deleteItem(item.data, item.id, item.type));
        }

        if (item.type === "ADD_USERS") deleteUser(item);

        if (item.type === "ADD_CATEGORIES") deleteCategories(item);

        if (item.type === "DELETE_PRODUCT") deleteProduct(item);

        dispatch({ type: "ADD_MODAL", payload: [] });
      }
    }
    closeModal();
  };

  const divStyle = {
    display: displayModal ? "block" : "none",
  };

  const stopP = (e) => {
    if (e) {
      e.stopPropagation();
    }
  };

  const closeModalInner = useCallback(() => {
    stopP();
    closeModal();
  }, [closeModal]);

  if (!displayModal) {
    return null;
  }

  return (
    <div className="modal" onClick={closeModalInner} style={divStyle}>
      <div className="modal-content" onClick={stopP}>
        <h5 className="modal-title text-capitalize" id="exampleModalLabel">
          {modal.length !== 0 && modal[0].title}
        </h5>

        <div className="modal-body">Do you want to delete this item?</div>

        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
            onClick={handleSubmit}
          >
            Yes
          </button>
          <button
            type="button"
            className="btn btn-primary"
            data-dismiss="modal"
            onClick={closeModalInner}
          >
            Cancel
          </button>
        </div>

        <span className="close" onClick={closeModalInner}>
          &times;
        </span>
      </div>
    </div>
  );
});
export default Modal;
