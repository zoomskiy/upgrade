import React, { useCallback, useEffect, useState } from "react";
import Modal from "./ModalPaymant";

const PaymentPage = ({ setUpdateCartCallback, total }) => {
  const [visibleModal, setVisibleModal] = useState(null);
  const handleCloseModal = useCallback(() => {
    setVisibleModal(null);
  }, []);

  const handleClickOpenSignIn = () => {
    if (visibleModal === null) {
      setVisibleModal("paymentOrder");
    }
  };

  return (
    <div onClick={handleClickOpenSignIn}>
      <div style={{ cursor: "pointer" }}>Перейти к оформлению заказа</div>
      <Modal
        displayModal={visibleModal === "paymentOrder"}
        total={total}
        closeModal={handleCloseModal}
        setUpdateCartCallback={setUpdateCartCallback}
      />
    </div>
  );
};

export default PaymentPage;
