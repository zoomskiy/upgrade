import React, { memo, useCallback, useContext, useEffect } from "react";
import { DataContext } from "../../store/context/globalState";
import { deleteItem } from "../../store/context/actions";
import { deleteData } from "../../utils/fetchData";
import StepperPage from "components/Stepper/StepperPage";

const Modal = memo(({ displayModal, closeModal, children, total, setUpdateCartCallback }) => {
  // const { state, dispatch } = useContext(DataContext);
  // const { modal, auth } = state;
  const divStyle = {
    display: displayModal ? "block" : "none",
    // zIndex: "9999",
  };

  const closeModalInner = useCallback(() => {
    closeModal();
  }, [closeModal]);

  if (!displayModal) {
    return null;
  }

  // const handleSubmit = () => {};

  return (
    <div className="modal" style={divStyle}>
      <div className="modal-content">
        <StepperPage
          setUpdateCartCallback={setUpdateCartCallback}
          total={total}
          closeModalInner={closeModalInner}
        />

        {/* <span className="close" onClick={closeModalInner}>
          &times; ЗАКРЫТИЕ
        </span> */}
      </div>
    </div>
  );
});
export default Modal;
