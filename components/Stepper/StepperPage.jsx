import React, { useEffect, useState } from "react";

import ShippingStep from "./components/ShippingStep";
import VariousPaymentStep2 from "./components/VariousPaymentStep2";
import clearFormData from "./services/clearFormData";
import setFormData from "./services/setFormData";
import getFormData from "./services/getFormData";
import clearActiveStep from "./services/clearActiveStep";
import getActiveStep from "./services/getActiveStep";
import setActiveStep from "./services/setActiveStep";
import ConfirmShippingStep from "./components/ConfirmShippingStep";
import SuccesPaymentC from "./components/SuccesPayment";
import {
  SHIPPING_STEP_PAGE,
  VARIOUS_PAYMENT_STEP,
  CONFIRM_SHIPPING_STEP,
  SuccesPayment,
} from "./constants";

const TABS = [
  {
    component: ShippingStep,
    title: "Доставка",
    stepBack: null,
    stepNext: VARIOUS_PAYMENT_STEP,
  },
  {
    component: VariousPaymentStep2,
    title: "Способ оплаты",
    stepBack: SHIPPING_STEP_PAGE,
    stepNext: CONFIRM_SHIPPING_STEP,
  },
  {
    component: ConfirmShippingStep,
    title: "Подтверждение заказа",
    stepBack: VARIOUS_PAYMENT_STEP,
    stepNext: 3,
  },
  {
    component: SuccesPaymentC,
    title: "Покупка",
    stepBack: null,
    stepNext: 0,
  },
];

const StepperPage = ({ setUpdateCartCallback, total, closeModalInner }) => {
  const [state, setstate] = useState(0);
  // const [loading, setloading] = useState(false);
  const [endedPaymant, setendedPaymant] = useState("");
  const tab = TABS[state];

  useEffect(() => {
    if (window) {
      const currentStep = getActiveStep();
      if (currentStep) {
        setstate(currentStep);
      } else {
        setActiveStep(state);
      }

      // setloading(true);
    }
  }, [endedPaymant, state]);

  const handleSteps = (stepNext, stepBack, variant) => {
    if (variant === "back") {
      setstate(stepBack);
      setActiveStep(stepBack);
    } else {
      setstate(stepNext);
      setActiveStep(stepNext);
    }
  };

  // if (!loading) {
  //   return "loading";
  // }

  return (
    <div>
      {tab && (
        <>
          <tab.component
            title={tab.title}
            handleSteps={(variant) => handleSteps.call(null, tab.stepNext, tab.stepBack, variant)}
            setUpdateCartCallback={setUpdateCartCallback}
            total={total}
            setstate={setstate}
            endedPaymant={[endedPaymant, setendedPaymant]}
            closeModalInner={closeModalInner}
          />

          {tab.title !== "Покупка" && (
            <span className="close" onClick={closeModalInner}>
              &times; ЗАКРЫТИЕ
            </span>
          )}
        </>
      )}
    </div>
  );
};

export default StepperPage;
