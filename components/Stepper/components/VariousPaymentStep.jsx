import React, { useCallback, useEffect, useState } from "react";
import { Form, Field } from "react-final-form";
import getFormData from "../services/getFormData";
import setActiveStep from "../services/setActiveStep";
import setFormData from "../services/setFormData";

const TextField = ({ input, meta, id, className, label }) => {
  return (
    <>
      <label htmlFor={id}>{label}</label>
      <input {...input} id={id} className={className} label={label} />
    </>
  );
};

const VariousPaymentStep = ({ setstate, title }) => {
  const [initialValues, setInitial] = useState({});
  const onSubmit = useCallback((values) => {
    const data = getFormData();
    setFormData({ ...data, 2: values });
    setActiveStep(2);
    setstate(2);
  }, []);

  useEffect(() => {
    if (window) {
      const data = getFormData();

      if (data) {
        if (data["2"]) {
          setInitial(data["2"]);
        } else {
          setInitial(data);
        }
      }
    }
  }, []);

  const handleBack = () => {
    setstate(0);
    setActiveStep(0);
  };
  return (
    <div>
      <h2>{title}</h2>
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="numberUser"
              id="numberUser"
              component={TextField}
              type="text"
              label="Номер телефона"
            />

            <div className="buttons">
              <button type="submit">Дальше</button>
            </div>
          </form>
        )}
      />
      <button onClick={handleBack}>Назад</button>
    </div>
  );
};

export default VariousPaymentStep;
