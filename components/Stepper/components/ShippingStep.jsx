import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import { Form, Field } from "react-final-form";
import getFormData from "../services/getFormData";
import setActiveStep from "../services/setActiveStep";
import setFormData from "../services/setFormData";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Input from "components/Form/Input/Input/Input";
import NumberFormat from "react-number-format";
import { ErrorMessage } from "@hookform/error-message";

// const TextField = ({ input, meta, id, className, label }) => {
//   return (
//     <>
//       <label style={{ textAlign: "left" }} htmlFor={id}>
//         {label}
//       </label>
//       <input {...input} id={id} className={className} label={label} />
//     </>
//   );
// };

const ShippingValidatorSchema = yup.object().shape({
  firstName: yup.string().required("Введите обязательно имя"),
  lastName: yup.string().required("Введите обязательно Фамилию"),
  email: yup.string().email("Неверная почта").required("Введите почту"),
  address: yup.string().required("Введите адрес"),
  phone: yup.string().required("Введите номер").min(11),
  shippingDec: yup.string(),
});

const ShippingStep = ({ title, handleSteps }) => {
  const {
    register,
    formState: { errors },
    reset,
    setValue,
    handleSubmit,
    setError,
    getValues,
    watch,
    control,
  } = useForm({
    resolver: yupResolver(ShippingValidatorSchema),
    reValidateMode: "onChange",
  });

  const watchEmail = watch("email");
  const watchFirstName = watch("firstName");
  const watchLastName = watch("lastName");
  const watchAddress = watch("address");
  const watchPhone = watch("phone");

  const onSubmit = useCallback((values) => {
    setFormData({ ...getFormData(), 1: values });
    handleSteps("next");
  }, []);

  useEffect(() => {
    if (window) {
      const data = getFormData();
      if (data["1"]) {
        const { firstName, lastName, phone, address, email, shippingDec } = data["1"];
        setValue("firstName", firstName);
        setValue("lastName", lastName);
        setValue("email", email);
        setValue("address", address);
        setValue("phone", phone);
        setValue("shippingDec", shippingDec || "");
      }
    }
  }, []);

  return (
    <div>
      <h2>{title}</h2>
      <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
        <Input
          type="text"
          placeholder="Имя"
          label="Имя"
          name="firstName"
          errorMessage={errors.firstName}
          ref={register}
        />
        <Input
          type="text"
          placeholder="Фамилия"
          label="Фамилия"
          name="lastName"
          errorMessage={errors.lastName}
          ref={register}
        />
        <Input
          type="text"
          placeholder="E-Mail"
          label="E-Mail"
          name="email"
          errorMessage={errors.email}
          ref={register}
        />
        <Input
          type="text"
          placeholder="Адрес"
          label="address"
          name="address"
          errorMessage={errors.address}
          ref={register}
        />
        <div className="input-container">
          <label className="input-label" htmlFor="phone">
            Телефон *
          </label>
          <Controller
            render={({ field: { onChange, value } }) => (
              <NumberFormat
                format="+# (###) ###-##-##"
                onValueChange={(v) => onChange(v.value)}
                placeholder="+7 (999) 333-22-11"
                mask="_"
                name="phone"
                id="phone"
                value={value}
              />
            )}
            name="phone"
            id="phone"
            control={control}
          />
          <ErrorMessage errors={errors} name="phone" as="span" className="error-display" />
          {/* <Input
            type="text"
            placeholder="Телефон"
            label="phone"
            name="phone"
            errorMessage={errors.phone}
            ref={register}
            onChange={(e) => {
              const isNumber = Number(e.target.value);
              if (!isNaN(isNumber)) {
                if (`${e.target.value}`.length > 11) {
                  setValue("phone", e.target.value);
                  setError("phone", {
                    type: "manual",
                    message: "Вы привысило",
                  });
                }
              } else {
                const t = parseInt(e.target.value);

                if (isNaN(t)) {
                  setValue("phone", "");
                } else {
                  if (`${e.target.value}`.length >= 11) {
                    setValue("phone", t);
                  }
                }
              }
            }}
          /> */}
        </div>
        <Input
          type="textarea"
          placeholder="Дополнительная информация"
          label="Дополнительная информация"
          name="shippingDec"
          errorMessage={errors.shippingDec}
          ref={register}
        />

        <button type="submit">Далее</button>
      </form>
    </div>
  );
};

export default ShippingStep;
