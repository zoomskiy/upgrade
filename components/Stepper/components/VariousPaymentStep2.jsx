import React, { useCallback, useEffect, useState } from "react";
import { Form, Field } from "react-final-form";
import getFormData from "../services/getFormData";
import setActiveStep from "../services/setActiveStep";
import setFormData from "../services/setFormData";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import MySelect from "components/Form/Select/Select";
import { CONFIRM_SHIPPING_STEP, SHIPPING_STEP_PAGE } from "../constants";

// const TextField = ({ input, meta, id, className, label }) => {
//   return (
//     <>
//       <label htmlFor={id}>{label}</label>
//       <input {...input} id={id} className={className} label={label} />
//     </>
//   );
// };

const VariousPaymentStep2 = ({ title, handleSteps }) => {
  const {
    register,
    formState: { errors },
    reset,
    setValue,
    handleSubmit,
  } = useForm({
    reValidateMode: "onChange",
  });

  useEffect(() => {
    if (window) {
      const data = getFormData();
      if (data["2"]) {
        setValue("paymentType", data["2"].paymentType);
      } else {
        setValue("paymentType", "online");
      }
    }
  }, []);

  const onSubm = useCallback((values) => {
    setFormData({ ...getFormData(), 2: values });
    handleSteps("next");
  }, []);

  return (
    <div>
      <h2>{title}</h2>
      <form autoComplete="off" onSubmit={handleSubmit(onSubm)}>
        {/*//TODO изменить тип селекта на другой (радио и тд) */}
        <MySelect
          labelTitle="Способ оплаты"
          id="paymentType"
          name="paymentType"
          errors={errors.paymentType}
          options={[
            { value: "online", label: "Онлайн оплата" },
            { value: "cash", label: "Наличные" },
          ]}
          ref={register}
          onChange={(e) => {
            setValue("paymentType", e.target.value);
            setFormData({ ...getFormData(), 2: { paymentType: e.target.value } });
          }}
        />
        <button type="submit">Далее</button>
      </form>

      <button onClick={() => handleSteps("back")}>Назад</button>
    </div>
  );
};

export default VariousPaymentStep2;
