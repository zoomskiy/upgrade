import React, { memo, useContext, useEffect, useRef, useState } from "react";
import getFormData from "../services/getFormData";
import setActiveStep from "../services/setActiveStep";
import { VARIOUS_PAYMENT_STEP } from "../constants";
import { DataContext } from "store/context/globalState";
import { getData, postData } from "utils/fetchData";

const ComponentsItem = {
  1: "Доставка",
  2: "Способ оплаты",
};

const ComponentShippingStep = {
  firstName: "Имя",
  lastName: "Фамилия",
  email: "Электронная почта",
  address: "Адресс ",
  phone: "Телефон",
  shippingDec: "Дополнительная информация",
  paymentType: "Тип оплаты",
};

const ConfirmShippingStep = memo(
  ({ setstate, title, setUpdateCartCallback, handleSteps, total, endedPaymant }) => {
    const { state, dispatch } = useContext(DataContext);
    const { cart, auth, orders } = state;
    const [_, setValue] = endedPaymant;

    const [end, setend] = useState();
    const [loading, setloading] = useState(false);
    const [errors, seterrors] = useState("");
    const ref = useRef(null);
    const [isCloased, setisCloased] = useState(false);

    const handleEndPayment = async () => {
      try {
        setloading((prev) => !prev);
        // TODO - проверить введены ли все данные и есть они в сессион перед отправкой
        let newCart = [];
        for (const item of cart) {
          const res = await getData(`products/${item._id}`);
          if (res.product.inStock - item.quantity >= 0) {
            newCart.push(item);
          }
        }

        if (newCart.length < cart.length) {
          setloading((prev) => !prev);
          setUpdateCartCallback((prev) => !prev);
          return dispatch({
            type: "NOTIFY",
            payload: {
              error: "The product is out of stock or the quantity is insufficient.",
            },
          });
        }

        const [
          { address, email, firstName, lastName, phone, shippingDec },
          { paymentType },
        ] = Object.values(end);

        const res = await postData(
          "order",
          { address, email, firstName, lastName, phone, shippingDec, paymentType, total, cart },
          auth.token
        );

        if (res.err) {
          setloading((prev) => !prev);
          return dispatch({ type: "NOTIFY", payload: { error: res.err } });
        }

        const sessionID = res.sessionToken;
        const newOrderID = res.newOrder._id;

        const win = window.open(
          `http://localhost:1333/transactionPaymentOrder/${sessionID + "-" + newOrderID}`,
          "Payment",
          "status=yes,toolbar=yes,menubar=yes,location=no,width=500,height=500"
        );

        const timer1 = setInterval(() => {
          if (win.closed) {
            // setisCloased(true)
            // if(isCloased){
            clearInterval(ref.current);
            setloading(false);
            setValue({ message: "Вы заказали заказ,но не оплатили его", orderID: newOrderID });
            setActiveStep(3);
            setstate(3);
            const newOrder = {
              ...res.newOrder,
              user: auth.user,
            };
            dispatch({ type: "ADD_ORDERS", payload: [...orders, newOrder] });
            // }

            // dispatch({ type: "ADD_CART", payload: [] });
            // console.log(res.newOrder, '       console.log(res.newOrder)')
            // console.log(res,'res')
            // console.log(res.newOrder,'res')
            // return router.push(`/order/${res.newOrder._id}`);
          }
        }, 100);
        ref.current = timer1;
      } catch (error) {
        seterrors(error);
        setloading((prev) => !prev);
      }
    };

    useEffect(() => {
      window.addEventListener("message", (data) => {
        // console.log(parseData,'parseData')
        // console.log(typeof data.data, 'typeof data.data')
        // console.log(data.data,'data-data')
        if (typeof data.data === "string") {
          // console.log(data.data,'s')
          const parseData = data.data.includes("end") && JSON.parse(data.data);
          if (parseData.end) {
            // setisCloased(false)
            handleSteps("next");
            setValue({ message: "Успешно оплачено", orderID: parseData.orderId });
            const newOrder = {
              ...parseData.orderProduct,
              user: auth.user,
            };
            dispatch({ type: "ADD_ORDERS", payload: [...orders, newOrder] });
            clearInterval(ref.current);
          }

          // dispatch({ type: "ADD_CART", payload: [] });
          // console.log(res.newOrder, '       console.log(res.newOrder)')
          // const newOrder = {
          //   ...res.newOrder,
          //   user: auth.user,
          // };
          // dispatch({ type: "ADD_ORDERS", payload: [...orders, newOrder] });
          // return router.push(`/order/${res.newOrder._id}`);
        }
      });
      return () => {
        clearInterval(ref.current);
        ref.current = null;
        setloading(false);
      };
    }, []);

    useEffect(() => {
      if (window) {
        const data = getFormData();
        setend(data);
      }
    }, []);

    if (loading) {
      return <div>loading</div>;
    }

    if (!end) {
      return "loading";
    }

    return (
      <div>
        {errors && <div style={{ backgroundColor: "red" }}>{JSON.stringify(errors)}</div>}
        <h2>{title}</h2>
        {Object.entries(end).map(([activeStep, data], idx) => {
          // console.log(Object.entries(data), "data");
          return (
            <div key={idx}>
              <div>{ComponentsItem[activeStep]}</div>

              <div>
                {Object.entries(data).map(([label, data], idx) => {
                  return (
                    <div key={idx}>
                      <span>{ComponentShippingStep[label]}</span> -
                      <span>
                        {data === "cash" ? "Наличные" : data === "online" ? "Карта" : data}
                      </span>
                    </div>
                  );
                })}
              </div>
            </div>
          );
        })}
        <div>
          <button onClick={() => handleSteps("back")}>Назад</button>
        </div>
        <button onClick={handleEndPayment} type="button" className="btn btn-secondary">
          Завершить и оплатить заказ
        </button>
      </div>
    );
  }
);

export default ConfirmShippingStep;
