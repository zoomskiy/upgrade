import React, { useCallback, useContext, useEffect, useState } from "react";
import { DataContext } from "store/context/globalState";
import clearActiveStep from "../services/clearActiveStep";
import clearFormData from "../services/clearFormData";
import setActiveStep from "../services/setActiveStep";
import { useRouter } from "next/router";

const SuccesPayment = ({ setstate, title, handleSteps, endedPaymant, closeModalInner }) => {
  const { state, dispatch } = useContext(DataContext);
  const router = useRouter();
  useEffect(() => {
    if (window) {
      clearFormData();
    }
  }, []);

  return (
    <div>
      <h2>{title}</h2>
      <div>{endedPaymant[0].message}</div>
      <div
        onClick={() => {
          setActiveStep(0);
          closeModalInner();
          // setstate(0);
          endedPaymant[1]("");
          router.push(`/order/${endedPaymant[0].orderID}`);
          dispatch({ type: "ADD_CART", payload: [] });
        }}
      >
        Закрыть
      </div>
    </div>
  );
};

export default SuccesPayment;
