import { STEPPER_ACTIVE_STEP } from "../constants";

export default () => {
  const activeStep = sessionStorage.getItem(STEPPER_ACTIVE_STEP);

  if (!activeStep) {
    return 0;
  }

  return activeStep;
};
