import { STEPPER_ACTIVE_STEP } from "../constants";

export default () => {
  sessionStorage.removeItem(STEPPER_ACTIVE_STEP);
};
