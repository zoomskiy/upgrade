import { STEPPER_ACTIVE_STEP } from "../constants";

export default (activeStep) => {
  sessionStorage.setItem(STEPPER_ACTIVE_STEP, activeStep);
};
