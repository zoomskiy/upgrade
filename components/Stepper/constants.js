export const STEPPER_FORM_DATA_KEY = "stepper-form-data";
export const STEPPER_ACTIVE_STEP = "stepper-active-step";
export const SHIPPING_STEP_PAGE = 0;
export const VARIOUS_PAYMENT_STEP = 1;
export const CONFIRM_SHIPPING_STEP = 2;
export const SuccesPayment = 3;
