import React, { useContext } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import classnames from "classnames";
import { DataContext } from "store/context/globalState";
import Cookies from "js-cookie";
import { ACTIONS } from "../../store/context/actions";

const constantsLinks = [
  {
    path: "/",
    title: "Главная",
  },
  {
    path: "/rooms",
    title: "Комнаты",
  },
  {
    path: "/chat",
    title: "Чат",
  },
  {
    path: "/stepper",
    title: "Степпер",
  },
  {
    path: "/contacts",
    title: "Контакты",
  },
];

export const Navbar = () => {
  const { state, dispatch } = useContext(DataContext);
  const router = useRouter();
  const isActive = (r: any) => (r === router.pathname ? "active" : "");
  const handleLogout = () => {
    Cookies.remove("refreshtoken", { path: "api/auth/accessToken" });
    // Cookies.remove("firstLogin", {});

    // window.localStorage.removeItem("firstLogin");
    dispatch({ type: ACTIONS.AUTH, payload: {} });
    dispatch({
      type: ACTIONS.NOTIFY,
      payload: {
        success: "You exit from account",
      },
    });
  };

  return (
    <div className="main-navigation">
      <h1 className="logo">
        <a className="logo__link">
          <span className="visually-hidden">Logo</span>
        </a>
      </h1>
      <div className="main-navigation__inner-wrap">
        <nav className="nav">
          <ul className="nav-wrapper">
            {constantsLinks.map(({ title, path }, idx) => {
              return (
                <li className="nav-wrapper__item" key={idx}>
                  <Link href={`${path}`}>
                    <a
                      className={classnames("nav-wrapper__item-link", {
                        "nav-wrapper__item-link--active": isActive(`${path}`),
                      })}
                    >
                      {title}
                    </a>
                  </Link>
                </li>
              );
            })}

            <li className="nav-wrapper__item">
              <Link href="/cart">
                <a
                  className={classnames("nav-wrapper__item-link", {
                    "nav-wrapper__item-link--active": isActive("/cart"),
                  })}
                >
                  Корзина: {state.cart.length}
                </a>
              </Link>
            </li>

            {!state.auth.token && (
              <>
                <li className="nav-wrapper__item">
                  <Link href="/signUp">
                    <a
                      className={classnames("nav-wrapper__item-link", {
                        "nav-wrapper__item-link--active": isActive("/signUp"),
                      })}
                    >
                      Регистрация
                    </a>
                  </Link>
                </li>
                <li className="nav-wrapper__item">
                  <Link href="/signIn">
                    <a
                      className={classnames("nav-wrapper__item-link", {
                        "nav-wrapper__item-link--active": isActive("/signIn"),
                      })}
                    >
                      Логин
                    </a>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
        <div className="profile__bar" style={{ display: "flex", flexDirection: "column" }}>
          <div>
            {state.user.role === "admin" && (
              <>
                <Link href="/users">
                  <a className="dropdown-item">Users</a>
                </Link>
                <Link href="/create">
                  <a className="dropdown-item">Products</a>
                </Link>
                <Link href="/categories">
                  <a className="dropdown-item">Categories</a>
                </Link>
              </>
            )}
          </div>

          {state.auth.token ? (
            <div>
              Пользователь:<strong>{state.auth.user.username}</strong>
              <img src={state.auth.user.avatar} width="50" height="50" />
              <div>
                <Link href={`/profile`}>
                  <a>В профиль</a>
                </Link>
                {/* <Link href={`/profile/${state.auth.user.id}`}>
                  <a>В профиль</a>
                </Link> */}
              </div>
              <button onClick={handleLogout}>Выйти</button>
            </div>
          ) : (
            "Привет, Аноним!"
          )}
        </div>
      </div>
    </div>
  );
};
