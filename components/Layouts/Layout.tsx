import React from "react";
import { Navbar } from "components/Layouts/Navbar";
import Notify from "../Notify";

type LayoutPropsType = {
  children: any;
};

export const Layout: React.FC<LayoutPropsType> = ({ children }) => {
  return (
    <div className="section-inner">
      <Navbar />
      <Notify />
      {children}
    </div>
  );
};
