import React, { useEffect, useState } from "react";

const Notification = ({ type, dispatch, id: idv4, title, message }) => {
  const [width, setwidth] = useState(0);
  const [intervalID, setintervalID] = useState(null);
  const [exit, setexit] = useState(false);

  const handleStartTimer = () => {
    const id = setInterval(() => {
      setwidth((prev) => {
        if (prev < 100) {
          return prev + 0.5;
        }

        clearTimeout(id);
        return prev;
      });
    }, 20);

    setintervalID(id);
  };

  const handlePauseTimer = () => {
    clearInterval(intervalID);
  };

  React.useEffect(() => {
    handleStartTimer();
  }, []);

  const handleCloseNotification = () => {
    handlePauseTimer();
    setexit(true);
    setTimeout(() => {
      dispatch({ type: "REMOVE_NOTOFICATION", idv4 });
    }, 400);
  };

  useEffect(() => {
    if (width === 100) {
      handleCloseNotification();
    }
  }, [width]);

  return (
    <div
      className={`notification-item ${type === "SUCCESS" ? "success" : "error"} ${
        exit ? "exit" : ""
      }`}
    >
      {title && <h2>{title}</h2>}
      <p>{message}</p>
      {/* <button onClick={() => setexit(true)}>Exit</button> */}
      <div className="bar" style={{ width: `${width}%` }} />
      <div onClick={() => handleCloseNotification()}>ЗАКРЫТЬ</div>
    </div>
  );
};

export default Notification;
