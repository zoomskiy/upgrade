const Toast = ({ msg, handleShow, bgColor }) => {
  return (
    <div>
      <div>
        <strong className="mr-auto text-light">{msg.title}</strong>

        <button type="button" data-dismiss="toast" style={{ outline: "none" }} onClick={handleShow}>
          x
        </button>
      </div>

      <div className="toast-body">{msg.msg}</div>
    </div>
  );
};

export default Toast;
