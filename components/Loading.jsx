const Loading = () => {
  return (
    <div className="wrapper__loading">
      <div className="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      Идёт загрузка
    </div>
  );
};

export default Loading;
