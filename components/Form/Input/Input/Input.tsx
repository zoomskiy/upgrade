import React, { ChangeEvent } from "react";

interface Props {
  className?: string;
  disabled?: boolean;
  errorMessage?: any;
  label?: string;
  name?: string;
  placeholder?: string;
  required?: boolean;
  type?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

export default React.forwardRef<any, Props>(
  (
    {
      disabled = false,
      errorMessage = "",
      onChange = () => {},
      label = "",
      name = "",
      placeholder = "",
      required = false,
      type = "text",
      className,
    },
    ref
  ) => {
    return (
      <div className="input-container">
        {label != null && (
          <label className="input-label" htmlFor={name}>
            {required ? `${label} *` : label}
          </label>
        )}
        <input
          className={
            errorMessage !== null ? errorMessage && `${className} error-display` : className
          }
          disabled={disabled}
          key={name}
          id={name}
          name={name}
          placeholder={placeholder}
          type={type}
          {...ref(name)}
          onChange={onChange}
        />
        {errorMessage !== null && (
          <p className="input-error-message">{errorMessage?.message && errorMessage.message}</p>
        )}
      </div>
    );
  }
);
