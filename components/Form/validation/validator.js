import * as yup from "yup";

const mandatory = "This field is required";
const requireMandatoryString = yup.string().required(mandatory);
const requireNumber = yup.lazy((value) => {
  if (Number.isNaN(parseFloat(value))) {
    return yup
      .string()
      .oneOf(["", undefined], "TypeError Number") // allow empty value
      .typeError("TypeError Number")
      .transform((val) => {
        return val === null ? "" : val;
      });
  }
  return yup.number().typeError("TypeError Number");
});
const requireMandatoryNumber = yup.number().typeError("This field is required").required(mandatory);

const mainSectionValidator = yup.object({
  name: requireMandatoryString,
  email: requireMandatoryString,
  dateOfBirth: requireMandatoryString,
});

export const errorMessageSchema = yup.object().shape({
  mainSection: mainSectionValidator,
});

export const loginSchema = yup.object().shape({
  phoneNumber: yup
    .string()
    .required("Обязательное поле")
    .matches(
      /[0]*\([0]*([0-9]{3}|0{1}((x|[0-9]){2}[0-9]{2}))\)\s*[0-9]{3,4}[- ][0-9]{2}\s*[0-9]{2}/,
      {
        message: "Должен начинаться с нуля и содержать 11 цифр.",
      }
    ),

  password: yup
    .string()
    .required("Обязательное поле")
    .matches(/^(?=.*\d)(?=.*[a-zA-Z]).{6,20}$/, {
      message: "Должен содержать от 6 до 20 символов и состоит из цифры и буквы.",
    }),
});

export const maskPhoneNumber = (phone) => {
  const x = phone.replace(/\D/g, "").match(/(\d?)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
  return !x[3]
    ? x[1] + x[2]
    : `${x[1]}(${x[2]}) ${x[3]}${x[4] ? ` ${x[4]}` : ""}${x[5] ? ` ${x[5]}` : ""}`;
};

const schema = yup.object().shape({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string().required("Last name is required"),
  email: yup.string().email().required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .min(5, "Password must have at least 5 characters"),
  repeatPassword: yup
    .string()
    .required("Password confiramtion is required")
    .min(5, "Password must have at least 5 characters")
    .oneOf([yup.ref("password"), null], "Passwords must match"),
});

export const schemaDefaultValues = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  repeatPassword: "",
};
