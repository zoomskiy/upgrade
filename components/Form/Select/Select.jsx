import React from "react";

export default React.forwardRef(({ labelTitle, id, name, errors, options, onChange }, ref) => {
  return (
    <div className="form-field">
      <label htmlFor={id} className="form-label">
        {labelTitle}
      </label>
      <select name={name} {...ref(name)} onChange={onChange}>
        {options.map(({ value, label }) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </select>
      <div>{errors && <span className="form-error">{errors.message}</span>}</div>
    </div>
  );
});
