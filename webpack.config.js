/*
КОНФИГУРАЦИЯ ВЕБПАКА
- Эта функция возвращает базовую конфигурацию веб-пакета, включенную в NextJS.
- Вы можете добавить к этому дополнительные настройки, плагины, правила и т. Д.
*/

const path = require("path");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");

module.exports = (
  config,
  { buildId, dev, isServer, defaultLoaders, webpack }
) => {
  const { ANALYZE } = process.env;
  /*
  Альясы:
  Allow you to import common folders as if they are modules, e.g.
  import { api } from 'tools'
  NOTE: This is only for the frontend
  */
  Object.assign(config.resolve.alias, {
    /*
    СТОРОНА КЛИЕНТА
    Формат модуля ES (синтаксис импорта / экспорта)
    */
    pages: path.resolve(__dirname, "pages/"),
    containers: path.resolve(__dirname, "containers/"),
    components: path.resolve(__dirname, "components/"),
    styles: path.resolve(__dirname, "styles/"),
    models: path.resolve(__dirname, "models/"),
    middleware: path.resolve(__dirname, "middleware/"),
    enums: path.resolve(__dirname, "enums/"),
    /*
    УНИВЕРСАЛЬНЫЙ
    Формат CommonJS (module.exports / require)
    */
    config: path.resolve(__dirname, "config/"),
    tools: path.resolve(__dirname, "tools/"), //  Названо потому, что 'util' зарезервирован
  });
  /*
  ИМПОРТ ФАЙЛОВ
  Позволяет указать шрифты, значки
  */
  config.module.rules.push({
    test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
    use: {
      loader: "url-loader",
      options: {
        limit: 100000,
      },
    },
  });
  /*
  АНАЛИЗАТОР:
  Добавляет анализатор пакетов webpack
  */
  if (ANALYZE) {
    config.plugins.push(
      new BundleAnalyzerPlugin({
        analyzerMode: "server",
        analyzerPort: isServer ? 8888 : 8889,
        openAnalyzer: true,
      })
    );
  }
  config.module.rules.push({
    test: /\.svg$/,
    use: ["@svgr/webpack"],
  });
  config.resolve.extensions = [".ts", ".js", ".jsx", ".tsx", ".svg", ".scss"];
  config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//));

  return config;
};
